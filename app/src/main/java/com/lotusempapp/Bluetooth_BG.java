package com.lotusempapp;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by ABC on 8/25/2016.
 */
public class Bluetooth_BG extends Service {

    private BluetoothAdapter mBluetoothAdapter;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter != null) {
            mBluetoothAdapter.enable();
        }
        if (!mBluetoothAdapter.isEnabled())
            mBluetoothAdapter.enable();


        return START_STICKY;
    }
}
