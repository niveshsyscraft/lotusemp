package com.lotusempapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lotusempapp.R;
import com.lotusempapp.adapters.UsersInfoAdapter;
import com.lotusempapp.apputils.PreferenceConnector;
import com.lotusempapp.httputils.AsyncTaskHandler;
import com.lotusempapp.httputils.CheckNetwork;
import com.lotusempapp.httputils.ResponseHandler;
import com.lotusempapp.httputils.UrlConfig;
import com.lotusempapp.models.UserPurchasedHistory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Nivesh on 20-Feb-17.
 */

public class UserDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<UserPurchasedHistory> usersDTOList;
    private RecyclerView rvUsersInfo;
    private UsersInfoAdapter rvUsersInfoAdapter;
    private TextView tvName,tvAddress;
    private Button btnFollowUp;
    private ImageButton btnBack;
    private String mobileNo;
    private String TAG = this.getClass().getSimpleName();
    private TextView heading;
    private LinearLayout coordinatorLayout;

    // Default getting Purchased history 9893035500


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userdetail);
        coordinatorLayout = (LinearLayout) findViewById(R.id.llHeader);
        coordinatorLayout.setVisibility(View.GONE);
        mobileNo = getIntent().getStringExtra("MobileNo");
        usersDTOList = new ArrayList<UserPurchasedHistory>();
        btnBack = (ImageButton) findViewById(R.id.btnBackButton);
        btnFollowUp = (Button) findViewById(R.id.btnFollowUp);
        tvName = (TextView) findViewById(R.id.tv_name);
        tvAddress = (TextView) findViewById(R.id.tv_address);
        heading = (TextView) findViewById(R.id.toolbar_title);
        String str = PreferenceConnector.readString(UserDetailActivity.this, PreferenceConnector.AssignedSection, "SectionName");
        heading.setText(str.toUpperCase());
        rvUsersInfo = (RecyclerView) findViewById(R.id.rvItem);
        rvUsersInfo.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvUsersInfo.setLayoutManager(mLayoutManager);
        btnBack.setOnClickListener(this);
        btnFollowUp.setOnClickListener(this);

        if(CheckNetwork.isNetwordAvailable(this))
            call();
        else
            showAlert("Internet connection not available.");
    }


    private void call ()
    {
        new AsyncTaskHandler(UserDetailActivity.this, UrlConfig.puchasedHistory+"9893035500", null, "GET", new ResponseHandler() {
            @Override
            public void handleResponse(String response) {
               try {
                   Log.e(TAG, response);
                   if (response != null) {
                       coordinatorLayout.setVisibility(View.VISIBLE);
                       getUserList(response);
                   } else {
                       showAlert("Error occurred while getting purchased list.");
                   }
               }
               catch (Exception e)
               {
                  e.printStackTrace();
                  showAlert("Error occurred while getting purchased list.");
               }
            }
        }).execute();

    }

    private void getUserList(String response) {
        boolean b=true,b1=true;
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("custinfoResult");
            if(jsonArray!=null && jsonArray .length()>0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    UserPurchasedHistory userPurchasedHistory = new UserPurchasedHistory();
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    String add = jsonObject1.optString("custadd1");
                    userPurchasedHistory.setCustadd1(add);
                    if (add != null && !add.equals("")) {
                        if (b1) {
                            tvAddress.setText(add);
                            b1 = false;
                        }
                    }

                    String add2 = jsonObject1.optString("custadd2");
                    userPurchasedHistory.setCustadd2(add2);
                    if (add2 != null && !add2.equals("")) {
                        if (b1) {
                            tvAddress.setText(add2);
                            b1 = false;
                        }
                    }

                    userPurchasedHistory.setCustcity(jsonObject1.optString("custcity"));
                    userPurchasedHistory.setCustcode(jsonObject1.optString("custcode"));
                    userPurchasedHistory.setCustemail(jsonObject1.optString("custemail"));
                    userPurchasedHistory.setCustmbl2(jsonObject1.optString("custmbl2"));
                    userPurchasedHistory.setCustmbl1(jsonObject1.optString("custmbl1"));
                    userPurchasedHistory.setCustname(jsonObject1.optString("custname"));
                    if (b) {
                        tvName.setText(jsonObject1.optString("custname"));
                        b = false;
                    }
                    userPurchasedHistory.setCustpho(jsonObject1.optString("custpho"));
                    userPurchasedHistory.setCustphr(jsonObject1.optString("custphr"));
                    userPurchasedHistory.setInvdt(jsonObject1.optString("invdt"));
                    userPurchasedHistory.setInvno(jsonObject1.optString("invno"));
                    userPurchasedHistory.setItemname(jsonObject1.optString("itemname"));
                    usersDTOList.add(userPurchasedHistory);
                }
                if (usersDTOList != null && usersDTOList.size() > 0) {
                    initRecycleView();
                } else {
                    showAlert("Purchased history not available.");
                }
            }
            else
            {
              showAlert("Purchased history not available.");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            showAlert("Purchased history not available.");
        }
    }

    private void initRecycleView() {

        rvUsersInfoAdapter = new UsersInfoAdapter(UserDetailActivity.this, usersDTOList);
        rvUsersInfo.setAdapter(rvUsersInfoAdapter);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.btnFollowUp :
                startActivity(new Intent(UserDetailActivity.this,FollowupActivity.class));
                break;
            case R.id.btnBackButton :
                finish();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private void showAlert(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alert");
        builder.setCancelable(false);
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        try {
            builder.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }

}
