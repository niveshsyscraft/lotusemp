package com.lotusempapp.activity;

import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.lotusempapp.R;
import com.lotusempapp.adapters.BeaconListAdapter;
import com.lotusempapp.apputils.AppUtil;
import com.lotusempapp.apputils.PreferenceConnector;
import com.lotusempapp.httputils.AsyncTaskHandler;
import com.lotusempapp.httputils.CheckNetwork;
import com.lotusempapp.httputils.ResponseHandler;
import com.lotusempapp.httputils.UrlConfig;
import com.lotusempapp.models.Beacons;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Nivesh on 09-Mar-17.
 */

public class BeaconListActivity extends AppCompatActivity {

    private TextView heading;
    private RecyclerView rvBeacons;
    private BeaconListAdapter beaconListAdapter;
    private List<Beacons> beaconsArrayList;
    private String TAG = this.getClass().getSimpleName();
    private ImageButton btnBackButton;
    private Toolbar toolbar;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beaconlist);
        toolbar = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter != null) {

            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        } else {
            // Device does not support Bluetooth
            AppUtil.showAlert("This device does not support bluetooth.", BeaconListActivity.this);
        }
        AppUtil.callPermission(BeaconListActivity.this);
        beaconsArrayList = new ArrayList<>();
        setTitle("");
        heading = (TextView) findViewById(R.id.toolbar_title);
        String str = PreferenceConnector.readString(BeaconListActivity.this, PreferenceConnector.Name, "UserName");
        heading.setText(str.toUpperCase());
        btnBackButton = (ImageButton) findViewById(R.id.btnBackButton);
        btnBackButton.setVisibility(View.GONE);
        rvBeacons = (RecyclerView) findViewById(R.id.beacon_list);
        rvBeacons.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvBeacons.setLayoutManager(mLayoutManager);
        if (CheckNetwork.isNetwordAvailable(this))
            getBeaconList();
        else
            showAlert("Internet connection not available,do you want to refresh.");
    }

    private void getBeaconList() {
        HashMap<String, String> param = new HashMap<>();
        String empID = PreferenceConnector.readString(this, PreferenceConnector.UserID, "id");
        if (CheckNetwork.isNetwordAvailable(this)) {
            if (!empID.equals("id")) {
                param.put("EmployeeID", empID);
                new AsyncTaskHandler(this, UrlConfig.getEmployeeDetails, param, "POST", new ResponseHandler() {
                    @Override
                    public void handleResponse(String response) {

                        try {
                            Log.e(TAG, response);
                            if (response != null && !response.equals("")) {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean isSuccess = jsonObject.optBoolean("IsSuccess");
                                Log.d(TAG, "" + isSuccess);

                                if (isSuccess) {
                                    String beacons_array = jsonObject.optString("beacons");
                                    PreferenceConnector.writeString(BeaconListActivity.this, PreferenceConnector.BEACONS_ARRAY, beacons_array);
                                    getBeaconList(beacons_array);
                                } else {
                                    showAlert("Beacon list not available,do you want to refresh.");
                                }
                            } else {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showAlert("Error occurred while getting beacon list, do you want to refresh.");
                        }

                    }
                }).execute();
            } else {
                showAlert("Session expired,please login Again.");
            }
        } else
            showAlert("Internet connection not available,do you want to refresh.");
    }

    private void getBeaconList(String beacons_array) {

        List<String> beaconIdList = new ArrayList<>();
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(beacons_array);
            if (jsonArray.length() != 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    Beacons beacons1 = new Beacons();
                    beacons1.set_id(jsonObject1.optString("_id"));
                    beacons1.setBeaconID(jsonObject1.optString("BeaconID"));
                    beacons1.setBeaconKey(jsonObject1.optString("BeaconKey"));
                    beacons1.setBeaconSection(jsonObject1.optString("BeaconSection"));
                    beacons1.setBeaconStore(jsonObject1.optString("BeaconStore"));
                    beacons1.setSectionID(jsonObject1.optString("SectionID"));
                    beaconsArrayList.add(beacons1);
                    if (!beaconIdList.contains(jsonObject1.optString("BeaconID"))) {
                        beaconIdList.add(beacons1.getBeaconID());
                        Log.e(TAG, beaconIdList.size() + "");
                    }
                }
            } else {
                showAlert("Beacon list not available.do you want to refresh.");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            showAlert("Error occurred while getting beacon list,do you want to refresh.");
        }
        if (beaconsArrayList != null && beaconsArrayList.size() != 0) {
            Log.e(TAG, "initilize");
            beaconListAdapter = new BeaconListAdapter(BeaconListActivity.this, beaconsArrayList);
            rvBeacons.setAdapter(beaconListAdapter);
        }
        PreferenceConnector.writeArraylist(BeaconListActivity.this, PreferenceConnector.ALLOTED_BEACON_MACID_LIST, beaconIdList);

    }


    private void showAlert(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alert");
        builder.setCancelable(false);
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getBeaconList();
            }
        })
                .setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
        try {
            builder.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Intent discoverableIntent =
                        new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
                startActivityForResult(discoverableIntent, 2);

            } else {
                Toast.makeText(this, "Please enable blutooth.", Toast.LENGTH_SHORT).show();
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }

        if (requestCode == 2) {
            if (resultCode == RESULT_CANCELED) {

                Toast.makeText(this, "Please enable discoverability of bluetooth.", Toast.LENGTH_SHORT).show();
                Intent discoverableIntent =
                        new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
                startActivityForResult(discoverableIntent, 2);
            }
        }
    }

    @Override
    public void onBackPressed() {
        /*AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alert");
        builder.setCancelable(false);
        builder.setMessage("Do you want to exit.");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        })
                .setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        try {
            builder.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
*/
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "click again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.beacon_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.today_follow) {
            startActivity(new Intent(BeaconListActivity.this, TodayFollowUPActivity.class));
            return true;
        }

        if (id == R.id.notification) {
            startActivity(new Intent(BeaconListActivity.this, Notificatio_list_Activity.class));
            return true;
        }

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

