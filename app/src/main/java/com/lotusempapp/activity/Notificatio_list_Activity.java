package com.lotusempapp.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.lotusempapp.R;
import com.lotusempapp.adapters.NotificationAdapter;
import com.lotusempapp.apputils.PreferenceConnector;
import com.lotusempapp.httputils.AsyncTaskHandler;
import com.lotusempapp.httputils.ResponseHandler;
import com.lotusempapp.httputils.UrlConfig;
import com.lotusempapp.models.Notification;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nivesh on 3/23/2017.
 */
public class Notificatio_list_Activity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton btnBackButton;
    private ArrayList<Notification> notificationList;
    private NotificationAdapter notificationAdapter;
    private RecyclerView rvNotification;
    private TextView heading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        btnBackButton = (ImageButton) findViewById(R.id.btnBackButton);
        btnBackButton.setOnClickListener(this);
        heading = (TextView) findViewById(R.id.toolbar_title);
        heading.setText("Notification");
        notificationList = new ArrayList<>();
        rvNotification = (RecyclerView) findViewById(R.id.notification_list);
        rvNotification.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvNotification.setLayoutManager(mLayoutManager);
        getNotificationList();
    }

    private void getNotificationList() {

        Map<String, String> param = new HashMap<>();
        param.put("emp_id", PreferenceConnector.readString(this, PreferenceConnector.UserID, ""));
        new AsyncTaskHandler(this, UrlConfig.NOTIFICATION_LIST, param, "POST", new ResponseHandler() {
            @Override
            public void handleResponse(String response) {
                try {
                    Log.e("Notificatio_list_Activity", response);

                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    if (jsonArray.length() != 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                            Notification notification = new Notification();
                            notification.setDate(jsonObject1.optString("notification_date"));
                            notification.setHeading(jsonObject1.optString("heading"));
                            notification.setIs_read(jsonObject1.optString("is_read"));
                            notification.setMessage(jsonObject1.optString("notification_message"));
                            notificationList.add(notification);
                        }

                        notificationAdapter = new NotificationAdapter(Notificatio_list_Activity.this, notificationList);
                        rvNotification.setAdapter(notificationAdapter);
                    } else {
                        showAlert("Notification is not available.", "finish");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    showAlert("Error occurred while getting notifications,do you want to refresh.", "option");
                }
            }
        }).execute();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBackButton:
                finish();
                break;
        }
    }

    private void showAlert(String msg, final String action) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alert");
        builder.setCancelable(false);
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (action.equalsIgnoreCase("option"))
                    getNotificationList();
                else
                    finish();
            }
        });
        if (action.equalsIgnoreCase("option")) {
            builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
        }
        try {
            builder.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }
}
