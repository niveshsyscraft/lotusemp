package com.lotusempapp.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lotusempapp.R;
import com.lotusempapp.adapters.UsersAdapter;
import com.lotusempapp.apputils.PreferenceConnector;
import com.lotusempapp.httputils.AsyncTaskHandler;
import com.lotusempapp.httputils.CheckNetwork;
import com.lotusempapp.httputils.ResponseHandler;
import com.lotusempapp.httputils.UrlConfig;
import com.lotusempapp.models.Customer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserListActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {

    private ArrayList<Customer> usersDTOList;
    private RecyclerView rvUsers;
    private UsersAdapter rvUsersAdapter;
    private EditText etSearch;
    private ImageView ivGetSearchResult;
    private ImageButton btnBackButton;
    private HashMap<String, String> param;
    private String TAG = UserListActivity.this.getClass().getSimpleName();
    private TextView heading;
    private LinearLayout searchBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userlist);

        usersDTOList = new ArrayList<>();
        heading = (TextView) findViewById(R.id.toolbar_title);
        String str = PreferenceConnector.readString(UserListActivity.this, PreferenceConnector.AssignedSection, "SectionName");
        heading.setText(str.toUpperCase());
     //   errorMsg = (TextView) findViewById(R.id.errorMsg);
        btnBackButton = (ImageButton) findViewById(R.id.btnBackButton);
        btnBackButton.setOnClickListener(this);
        etSearch = (EditText) findViewById(R.id.etSearch);
        ivGetSearchResult = (ImageView) findViewById(R.id.ivGetSearchResult);
        ivGetSearchResult.setOnClickListener(this);
        etSearch.addTextChangedListener(this);
        rvUsers = (RecyclerView) findViewById(R.id.rvItem);
        rvUsers.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvUsers.setLayoutManager(mLayoutManager);
   //     errorMsg.setVisibility(View.GONE);
   //     errorMsg.setOnClickListener(this);
        searchBar = (LinearLayout) findViewById(R.id.linearLayout);
        searchBar.setVisibility(View.GONE);
        if(CheckNetwork.isNetwordAvailable(UserListActivity.this)) {
              getUserList();
         /*   usersDTOList.add(new Customer("nivesh","9993911"));
            usersDTOList.add(new Customer("rajat","86597845"));
            usersDTOList.add(new Customer("pankaj","7548651"));
            usersDTOList.add(new Customer("nishchal","2554121"));
            usersDTOList.add(new Customer("nitin","31245679"));
            usersDTOList.add(new Customer("prakash","75468912435"));
            usersDTOList.add(new Customer("saket","12345769"));
            usersDTOList.add(new Customer("ali","45784652"));
            usersDTOList.add(new Customer("prasad","78455689"));
            usersDTOList.add(new Customer("sankalp","985467498"));
            rvUsersAdapter = new UsersAdapter(this,usersDTOList);
            rvUsers.setAdapter(rvUsersAdapter);*/
        }
        else
        {
  //        errorMsg.setText("Internet connection is not available");
   //       errorMsg.setVisibility(View.VISIBLE);
          searchBar.setVisibility(View.GONE);
          showAlert("Internet connection not available");
        }
    }

    private void getUserList() {

        param = new HashMap<>();
        String id = getIntent().getExtras().getString("beaconId");

        param.put("BeaconID", id);
        param.put("StoreID", "");
        param.put("UserID", PreferenceConnector.readString(UserListActivity.this, PreferenceConnector._id, "userId"));

        new AsyncTaskHandler(UserListActivity.this, UrlConfig.USERLIST_URL, param, "POST", new ResponseHandler() {
            @Override
            public void handleResponse(String response) {
                if(response!=null)
                    Log.d(TAG, response);
                if (response != null && !response.trim().equals("[]") && !response.trim().equals("[]\n")) {
                    rvUsers.setVisibility(View.VISIBLE);
                    initRecycleView(response);
            //        errorMsg.setVisibility(View.GONE);
                    searchBar.setVisibility(View.VISIBLE);

                } else {
            //        errorMsg.setText("User Not Available Yet please try again.");
            //        errorMsg.setVisibility(View.VISIBLE);
                    searchBar.setVisibility(View.GONE);
                    rvUsers.setVisibility(View.GONE);
                    etSearch.setEnabled(false);
                    showAlert("User(s) not available yet.");
                }
            }
        }).execute();
    }


    private void initRecycleView(String response) {

        Log.d(TAG, "initRecycleView");
        Log.e(TAG, response);
        covertJsonArrayToUserList(response);
        rvUsersAdapter = new UsersAdapter(UserListActivity.this, usersDTOList);
        rvUsers.setAdapter(rvUsersAdapter);
    }

    private void covertJsonArrayToUserList(String response) {

        try {
            JSONArray jsonArray = new JSONArray(response);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Customer customer = new Customer();
                customer.setBeaconKey(jsonObject.optString("BeaconKey"));
                customer.setBeaconID(jsonObject.optString("BeaconID"));
                customer.setDeviceID(jsonObject.optString("DeviceID"));
                customer.setDeviceName(jsonObject.optString("DeviceName"));
                customer.setMobileNo(jsonObject.optString("MobileNo"));
                customer.setDevicePhone(jsonObject.optString("DevicePhone"));
                customer.setUniqueKey(jsonObject.optString("UniqueKey"));
                customer.setDistance(jsonObject.optString("Distance"));
                Log.e(TAG,customer.toString());
                usersDTOList.add(customer);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            showAlert("Error occurred while getting user(s) list.");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBackButton:
                finish();
                break;
        /*    case R.id.errorMsg:
                getUserList();
                break;*/
            case R.id.ivGetSearchResult :
                View viewq = this.getCurrentFocus();
                if (viewq != null) {
                    InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(viewq.getWindowToken(), 0);
                }
                break;
        }
    }

    private void showAlert(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alert");
        builder.setCancelable(false);
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        try {
            builder.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
       String s = editable.toString().toLowerCase();
        List<Customer> list = new ArrayList<>();
       for(int i=0;i<usersDTOList.size();i++)
       {
          String s1 = usersDTOList.get(i).getDeviceName().toLowerCase();
          String s2 = usersDTOList.get(i).getDevicePhone();
          if(s1.contains(s) || s2.contains(s))
          {
            list.add(usersDTOList.get(i));
          }
       }
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvUsers.setLayoutManager(mLayoutManager);
        rvUsersAdapter = new UsersAdapter(this,list);
        rvUsers.setAdapter(rvUsersAdapter);
        rvUsersAdapter.notifyDataSetChanged();
    }
}
