package com.lotusempapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.lotusempapp.R;
import com.lotusempapp.adapters.TodayFollowAdapter;
import com.lotusempapp.apputils.PreferenceConnector;
import com.lotusempapp.httputils.AsyncTaskHandler;
import com.lotusempapp.httputils.CheckNetwork;
import com.lotusempapp.httputils.ResponseHandler;
import com.lotusempapp.httputils.UrlConfig;
import com.lotusempapp.models.TodayFollowUP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Nivesh on 16-Mar-17.
 */

public class TodayFollowUPActivity extends AppCompatActivity implements View.OnClickListener {

    private TodayFollowAdapter todayFollowAdapter;
    private List<TodayFollowUP> userlist;
    private TextView heading;
    private ImageButton btnBackButton;
    private String TAG = this.getClass().getSimpleName();
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_today_follow_up);
        setUpView();
    }

    private void setUpView() {
        recyclerView = (RecyclerView) findViewById(R.id.recycle_today_follow);
        heading = (TextView) findViewById(R.id.toolbar_title);
        heading.setText("TODAY FOLLOWUP");
        btnBackButton = (ImageButton) findViewById(R.id.btnBackButton);
        btnBackButton.setOnClickListener(this);
        userlist = new ArrayList<>();
        if(CheckNetwork.isNetwordAvailable(this))
          getUserList();
        else
          showAlert("Internet connection not available.","");
    }

    private void getUserList() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DATE);
        HashMap<String,String> param = new HashMap<>();
        String id = PreferenceConnector.readString(this,PreferenceConnector.UserID,"");
        if(!id.equals("")) {
            param.put("emp_id", id);
        //    param.put("follow_up_date",year+"-"+month+"-"+day);
            param.put("follow_up_date","2016-03-15");
            new AsyncTaskHandler(this, UrlConfig.TODAY_FOLLOW, param, "POST", new ResponseHandler() {
                @Override
                public void handleResponse(String response) {
                    Log.e(TAG,response);
                    if(response!=null && !response.equals(""))
                    {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String error = jsonObject.optString("error");
                            if(!error.equals("false")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for(int i=0;i<jsonArray.length();i++)
                                {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    TodayFollowUP followUP = new TodayFollowUP();
                                    followUP.setCreate_date(jsonObject1.optString("create_date"));
                                    followUP.setEmp_comments(jsonObject1.optString("emp_comments"));
                                    followUP.setId(jsonObject1.optString("emp_id"));
                                    followUP.setMobile_no(jsonObject1.optString("mobile_no"));
                                    followUP.setStatus(jsonObject1.optString("status"));
                                    followUP.setTentative_date(jsonObject1.optString("tentative_date"));
                                    followUP.setName(jsonObject1.optString("name"));
                                    userlist.add(followUP);
                                    Log.d(TAG,userlist.size()+"");
                                }

                                recyclerView.setHasFixedSize(true);
                                LinearLayoutManager mLayoutManager = new LinearLayoutManager(TodayFollowUPActivity.this);
                                recyclerView.setLayoutManager(mLayoutManager);
                                todayFollowAdapter = new TodayFollowAdapter(TodayFollowUPActivity.this,userlist);
                                recyclerView.setAdapter(todayFollowAdapter);
                            }
                            else
                            {
                               showAlert("No record found.","");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            showAlert("Error occurred while getting user(s) list.","");
                        }
                    }
                }
            }).execute();
        }
        else
        {
          showAlert("Session expired please login again.","stop");
        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.btnBackButton :
                finish();
                break;
        }
    }

    private void showAlert(String msg, final String stop) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alert");
        builder.setCancelable(false);
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!stop.equals("stop"))
                  finish();
                else
                {
                  startActivity(new Intent(TodayFollowUPActivity.this,LoginActivity.class));
                  finish();
                }
            }
        });
        try {
            builder.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }
}
