package com.lotusempapp.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.lotusempapp.R;
import com.lotusempapp.apputils.AppUtil;
import com.lotusempapp.apputils.PreferenceConnector;
import com.lotusempapp.httputils.AsyncTaskHandler;
import com.lotusempapp.httputils.CheckNetwork;
import com.lotusempapp.httputils.ResponseHandler;
import com.lotusempapp.httputils.UrlConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Dilip on 2/20/2017.
 */

public class FollowupActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etName,etMobileNumber,etProductName,etUserID,comment;
    private Button btnUpdateInCRM;
    ImageButton btnBackButton;
    private TextView heading;
    private int year,day,month;
    private TextView etTdop;
    private String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followup);
        setUpView();
    }

    private void setUpView() {

        heading = (TextView) findViewById(R.id.toolbar_title);
        heading.setText("FOLLOW UP");
        btnBackButton = (ImageButton) findViewById(R.id.btnBackButton);
        etName = (EditText) findViewById(R.id.etName);
        etTdop = (TextView) findViewById(R.id.etTdop);
        etMobileNumber = (EditText) findViewById(R.id.etMobileNo);
        comment = (EditText) findViewById(R.id.et_comment_follow);
        etProductName = (EditText) findViewById(R.id.etProductName);
        etUserID = (EditText) findViewById(R.id.et_userID_follow);
        btnUpdateInCRM = (Button) findViewById(R.id.btnUpdateincrm);
        btnUpdateInCRM.setOnClickListener(this);
        btnBackButton.setOnClickListener(this);
        etTdop.setOnClickListener(this);
        Calendar cal = Calendar.getInstance();
        year = cal.get(Calendar.YEAR);
        month = cal.get(Calendar.MONTH);
        day = cal.get(Calendar.DATE);
        etUserID.setText(PreferenceConnector.readString(this,PreferenceConnector.UserID,"id"));
        etUserID.setEnabled(false);
        etMobileNumber.setText(PreferenceConnector.readString(this,"custno",""));
        etName.setText(PreferenceConnector.readString(this,"custname",""));
    }

    @Override
    public void onClick(View view) {
       switch (view.getId())
       {
           case R.id.btnBackButton :
               finish();
               break;
           case R.id.etTdop :
               callDataDialog();
               break;
           case R.id.btnUpdateincrm :
               if(CheckNetwork.isNetwordAvailable(this)) {
                   View viewq = this.getCurrentFocus();
                   if (viewq != null) {
                       InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
                       imm.hideSoftInputFromWindow(viewq.getWindowToken(), 0);
                   }
                   Log.e(TAG,"call");
                   if (checkData()) {
                       sendData();
                   } else {
                   }
               }
               else
               {
                   AppUtil.showAlert("internet connection is not available.",this);
               }
               break;
       }
    }

    private void sendData() {
        Log.e(TAG,"send data");
        HashMap<String,String> param = new HashMap<>();
        param.put("name",etName.getText().toString());
        param.put("mobile_no",etMobileNumber.getText().toString());
        param.put("product_name",etProductName.getText().toString());
        param.put("tentative_date",etTdop.getText().toString());
        param.put("emp_id",etUserID.getText().toString());
        param.put("emp_comments",comment.getText().toString());
        new AsyncTaskHandler(this, UrlConfig.FOLLOW_UP, param, "POST", new ResponseHandler() {
            @Override
            public void handleResponse(String response) {
                Log.e(TAG,response);
                if(response!=null && !response.equals(""))
                {
                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String error = jsonObject.optString("error");
                        String msg = jsonObject.optString("message");
                        if(error.equals("") && msg.equals("User follow up taken successfully."))
                        {
                          showAlert("Information successfully uploaded.");
                        }
                        else
                        {
                          AppUtil.showAlert("Information is not uploaded",FollowupActivity.this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                  AppUtil.showAlert("Information is not send",FollowupActivity.this);
                }
            }
        }).execute();
    }

    private boolean checkData() {

        if(!etName.getText().toString().equals(""))
        {
           if(!etMobileNumber.getText().toString().equals(""))
           {
              if(!etProductName.getText().toString().equals(""))
              {
                 if(!etTdop.getText().toString().equals(""))
                 {
                    if(!etUserID.getText().toString().equals(""))
                     {
                        if(!comment.getText().toString().equals(""))
                        {
                            return true;
                        }
                         else
                        {
                            AppUtil.showAlert("Please enter comment.",this);
                        }
                     }
                     else
                     {
                         AppUtil.showAlert("Please enter userID.",this);
                     }
                 }
                  else
                 {
                     AppUtil.showAlert("Please select date.",this);
                 }
              }
               else
              {
                  AppUtil.showAlert("Please enter product name.",this);
              }
           }
           else
           {
               AppUtil.showAlert("Please enter mobile number.",this);
           }
        }
        else
        {
            AppUtil.showAlert("Please enter name.",this);
        }
        return false;
    }

    private void callDataDialog() {

        DatePickerDialog datePickerDialog = new DatePickerDialog(FollowupActivity.this,myDateListener,year,month,day);
        datePickerDialog.setCancelable(false);
        datePickerDialog.setTitle("Select the date");
        datePickerDialog.show();
    }


    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

            i1 += 1;
            etTdop.setText(i+"-"+i1+"-"+i2);
        }
    };

    private void showAlert(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alert");
        builder.setCancelable(false);
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               finish();
            }
        });
        try {
            builder.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }

}
