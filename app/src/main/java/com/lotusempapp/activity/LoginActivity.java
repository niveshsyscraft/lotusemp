package com.lotusempapp.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.lotusempapp.R;
import com.lotusempapp.apputils.AppUtil;
import com.lotusempapp.apputils.PreferenceConnector;
import com.lotusempapp.httputils.AsyncTaskHandler;
import com.lotusempapp.httputils.CheckNetwork;
import com.lotusempapp.httputils.ResponseHandler;
import com.lotusempapp.httputils.UrlConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    final String TAG = this.getClass().getSimpleName();
    private EditText etUsername, etPassword;
    private Button btlogin;
    private String beacons_array;
    String Firebasetoken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        AppUtil.callPermission(LoginActivity.this);
        setUpView();

    }

    private void setUpView() {
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        setHint();
        btlogin = (Button) findViewById(R.id.btnLogin);
        btlogin.setOnClickListener(this);
    }

    private void setHint() {
        etUsername.setHint("user123");
        etPassword.setHint("*****");
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnLogin:
                if (CheckNetwork.isNetwordAvailable(LoginActivity.this)) {
                    if (checkFirebaseToken()) {
                        if (checkLoginCredentials()) {
                            View viewq = this.getCurrentFocus();
                            if (viewq != null) {
                                InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(viewq.getWindowToken(), 0);
                            }

                            HashMap<String, String> param = new HashMap<>();
                            param.put("username", etUsername.getText().toString().trim());
                            param.put("password", etPassword.getText().toString().trim());
                            param.put("fromApp", "1");
                            param.put("devicetoken", Firebasetoken);

                            new AsyncTaskHandler(LoginActivity.this,
                                    UrlConfig.LOGIN_URL, param, "POST", new ResponseHandler() {

                                JSONObject jsonObject;
                                String message, _id, name, UserID, Designation, AssignedStore, AssignedSection;
                                int UserType;
                                boolean isSuccess;

                                @Override
                                public void handleResponse(String response) {
                                    Log.d(TAG, response);
                                    if (response != null && !response.equals("")) {
                                        try {
                                            jsonObject = new JSONObject(response);
                                            message = jsonObject.optString("message");
                                            AssignedSection = jsonObject.optString("SectionName");
                                            beacons_array = jsonObject.optString("beacons");
                                            isSuccess = jsonObject.optBoolean("isSuccess");
                                            if (message.equalsIgnoreCase("Successfully loggedin.") && isSuccess) {
                                                Toast.makeText(LoginActivity.this, "loged in..", Toast.LENGTH_SHORT).show();
                                                JSONObject jsonObject1 = jsonObject.getJSONObject("user");
                                                _id = jsonObject1.optString("_id");
                                                UserID = jsonObject1.optString("UserID");
                                                name = jsonObject1.optString("Name");
                                                Designation = jsonObject1.optString("Designation");
                                                AssignedStore = jsonObject1.optString("AssignedStore");

                                                UserType = jsonObject1.optInt("UserType");
                                                PreferenceConnector.writeBoolean(LoginActivity.this, PreferenceConnector.IS_LOGIN, true);
                                                PreferenceConnector.writeString(LoginActivity.this, PreferenceConnector._id, _id);
                                                PreferenceConnector.writeString(LoginActivity.this, PreferenceConnector.UserID, UserID);
                                                PreferenceConnector.writeString(LoginActivity.this, PreferenceConnector.Name, name);
                                                PreferenceConnector.writeString(LoginActivity.this, PreferenceConnector.Designation, Designation);
                                                PreferenceConnector.writeString(LoginActivity.this, PreferenceConnector.AssignedStore, AssignedStore);
                                                PreferenceConnector.writeString(LoginActivity.this, PreferenceConnector.BEACONS_ARRAY, beacons_array);
                                                PreferenceConnector.writeInteger(LoginActivity.this, PreferenceConnector.UserType, UserType);
                                                PreferenceConnector.writeString(LoginActivity.this, PreferenceConnector.AssignedSection, AssignedSection);
                                                startActivity(new Intent(LoginActivity.this, BeaconListActivity.class));
                                                finish();
                                            } else {
                                                etPassword.setText("");
                                                if(message.equals("Invalid Username."))
                                                {
                                                  AppUtil.showAlert("You entered invalid username.",LoginActivity.this);
                                                }
                                                else
                                                    if(message.equals("Wrong Password."))
                                                    {
                                                        AppUtil.showAlert("You entered invalid password.",LoginActivity.this);
                                                    }
                                                else
                                                        AppUtil.showAlert("Username password invalid are invalid..",LoginActivity.this);
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }).execute();
                        }

                    } else {
                    }
                }
                else
                AppUtil.showAlert("Internet connection not available.",this);
        }
    }

    private boolean checkFirebaseToken() {

        String token1 = FirebaseInstanceId.getInstance().getToken();
        String token = PreferenceConnector.readString(LoginActivity.this, PreferenceConnector.FIREBASE_TOKEN, "deviceToken");
        if (token1 != null) {
            Firebasetoken = token1;
            return true;
        } else if (!token.equalsIgnoreCase("deviceToken")) {
            Firebasetoken = token;
            return true;
        } else {
            showAlert(this);
            return false;
        }
    }

    private void showAlert(final LoginActivity loginActivity) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(loginActivity);
        builder.setTitle("Alert");
        builder.setCancelable(false);
        builder.setMessage("Your firebase token is not register,Please restart application");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                loginActivity.startActivity(new Intent(loginActivity, SplashActivity.class));
                loginActivity.finish();
            }
        });
        try {
            builder.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
    }

    private boolean checkLoginCredentials() {

        if (!etUsername.getText().toString().equals("")) {
            if (!etPassword.getText().toString().equals("")) {
                return true;
            }
            else
            AppUtil.showAlert("Please enter password.", this);
        } else {
            AppUtil.showAlert("Please enter username.", this);
        }
        return false;
    }

}
