package com.lotusempapp.models;

/**
 * Created by Nivesh on 16-Mar-17.
 */

public class TodayFollowUP {

    String id,name,mobile_no,product_name,tentative_date,emp_comments,status,create_date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String product_name() {
        return product_name;
    }

    public void product_name(String product_name) {
        this.product_name = product_name;
    }

    public String getTentative_date() {
        return tentative_date;
    }

    public void setTentative_date(String tentative_date) {
        this.tentative_date = tentative_date;
    }

    public String getEmp_comments() {
        return emp_comments;
    }

    public void setEmp_comments(String emp_comments) {
        this.emp_comments = emp_comments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }
}
