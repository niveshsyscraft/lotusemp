package com.lotusempapp.models;

/**
 * Created by syscraft on 3/9/2017.
 */

public class Customer {

    String _id;
    String BeaconID;
    String DeviceID;
    String MobileNo;
    String Distance;
    String connectiontime;
    String BeaconKey;
    String UniqueKey;
    String DeviceName;
    String DevicePhone;

    public Customer() {
    }

    public Customer(String deviceName, String devicePhone) {
        DeviceName = deviceName;
        DevicePhone = devicePhone;
    }

    public String getBeaconID() {
        return BeaconID;
    }

    public void setBeaconID(String beaconID) {
        BeaconID = beaconID;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(String deviceID) {
        DeviceID = deviceID;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public String getConnectiontime() {
        return connectiontime;
    }

    public void setConnectiontime(String connectiontime) {
        this.connectiontime = connectiontime;
    }

    public String getBeaconKey() {
        return BeaconKey;
    }

    public void setBeaconKey(String beaconKey) {
        BeaconKey = beaconKey;
    }

    public String getUniqueKey() {
        return UniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        UniqueKey = uniqueKey;
    }

    public String getDeviceName() {
        return DeviceName;
    }

    public void setDeviceName(String deviceName) {
        DeviceName = deviceName;
    }

    public String getDevicePhone() {
        return DevicePhone;
    }

    public void setDevicePhone(String devicePhone) {
        DevicePhone = devicePhone;
    }


    public String toString()
    {
      return getMobileNo()+""+getDeviceName()+""+getDistance()+""+getBeaconID();
    }

}
