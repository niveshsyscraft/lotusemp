package com.lotusempapp.models;

/**
 * Created by Dilip on 2/20/2017.
 */

public class Users {

    private String userName;
    private String userAddress;
    private String userMobileNo;

    public Users(String userName, String userAddress, String userMobileNo) {
        this.userName = userName;
        this.userAddress = userAddress;
        this.userMobileNo = userMobileNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserMobileNo() {
        return userMobileNo;
    }

    public void setUserMobileNo(String userMobileNo) {
        this.userMobileNo = userMobileNo;
    }
}
