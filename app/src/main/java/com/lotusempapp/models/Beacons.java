package com.lotusempapp.models;

/**
 * Created by Nivesh on 09-Mar-17.
 */

public class Beacons {

    String BeaconKey,_id,BeaconID,BeaconStore,SectionID,BeaconSection;


    public Beacons() {
    }

    public String getBeaconKey() {
        return BeaconKey;
    }

    public void setBeaconKey(String beaconKey) {
        BeaconKey = beaconKey;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getBeaconID() {
        return BeaconID;
    }

    public void setBeaconID(String beaconID) {
        BeaconID = beaconID;
    }

    public String getBeaconStore() {
        return BeaconStore;
    }

    public void setBeaconStore(String beaconStore) {
        BeaconStore = beaconStore;
    }

    public String getSectionID() {
        return SectionID;
    }

    public void setSectionID(String sectionID) {
        SectionID = sectionID;
    }

    public String getBeaconSection() {
        return BeaconSection;
    }

    public void setBeaconSection(String beaconSection) {
        BeaconSection = beaconSection;
    }
}
