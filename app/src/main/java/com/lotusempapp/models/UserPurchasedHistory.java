package com.lotusempapp.models;

/**
 * Created by Nivesh on 14-Mar-17.
 */

public class UserPurchasedHistory {

    String custadd1,custadd2,custcity,custcode,custemail,custmbl1,custmbl2,custname,custpho,custphr,invdt,invno,itemname;

    public String getCustadd1() {
        return custadd1;
    }

    public void setCustadd1(String custadd1) {
        this.custadd1 = custadd1;
    }

    public String getCustadd2() {
        return custadd2;
    }

    public void setCustadd2(String custadd2) {
        this.custadd2 = custadd2;
    }

    public String getCustcity() {
        return custcity;
    }

    public void setCustcity(String custcity) {
        this.custcity = custcity;
    }

    public String getCustcode() {
        return custcode;
    }

    public void setCustcode(String custcode) {
        this.custcode = custcode;
    }

    public String getCustemail() {
        return custemail;
    }

    public void setCustemail(String custemail) {
        this.custemail = custemail;
    }

    public String getCustmbl1() {
        return custmbl1;
    }

    public void setCustmbl1(String custmbl1) {
        this.custmbl1 = custmbl1;
    }

    public String getCustmbl2() {
        return custmbl2;
    }

    public void setCustmbl2(String custmbl2) {
        this.custmbl2 = custmbl2;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getCustpho() {
        return custpho;
    }

    public void setCustpho(String custpho) {
        this.custpho = custpho;
    }

    public String getCustphr() {
        return custphr;
    }

    public void setCustphr(String custphr) {
        this.custphr = custphr;
    }

    public String getInvdt() {
        return invdt;
    }

    public void setInvdt(String invdt) {
        this.invdt = invdt;
    }

    public String getInvno() {
        return invno;
    }

    public void setInvno(String invno) {
        this.invno = invno;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }
}
