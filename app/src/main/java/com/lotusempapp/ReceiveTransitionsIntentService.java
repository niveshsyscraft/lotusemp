package com.lotusempapp;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
//import com.lotusapp.SplashScreen;
import com.lotusempapp.activity.SplashActivity;

import java.util.ArrayList;
import java.util.List;


public class ReceiveTransitionsIntentService extends IntentService {

    String regid,distance;

    private BluetoothAdapter mBluetoothAdapter;

    String TAG="ReceiveTransitionsIntentService";

    public ReceiveTransitionsIntentService() {

        super(ReceiveTransitionsIntentService.class.getSimpleName());
        Log.e("Pankaj","ReceiveTransitionsIntentService :--->>>");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        getTransitionString(Geofence.GEOFENCE_TRANSITION_ENTER);
         regid = intent.getStringExtra("regid");
         distance = intent.getStringExtra("distance");

        Log.e("Pankaj","ReceiveTransitionsIntentService :--->>>"+regid+"   distance :--->"+distance);

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
//            String errorMessage = GeofenceErrorMessages.getErrorString(this,
//                    geofencingEvent.getErrorCode());
//            Log.e(TAG, errorMessage);
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            Log.v(TAG, "GEOFENCE_TRANSITION_ENTER ");
//            Thread t = new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    new SendDistance().execute();
//                }
//            });
//            t.start();

            List triggeringGeofences = geofencingEvent.getTriggeringGeofences();

            // Get the transition details as a String.
            String geofenceTransitionDetails = getGeofenceTransitionDetails(
                    this,
                    geofenceTransition,
                    triggeringGeofences
            );

            // Send notification and log the transition details.
//            sendNotification(geofenceTransitionDetails);
            Log.i("GEOFENCE_TRANSITION_ENTER", geofenceTransitionDetails);
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter != null) {
                mBluetoothAdapter.enable();
            }
            if (!mBluetoothAdapter.isEnabled())
                mBluetoothAdapter.enable();

        } else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            Log.v(TAG, "GEOFENCE_TRANSITION_EXIT ");
            AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent intent1 = new Intent(this, Bluetooth_BG.class);
            PendingIntent alarmIntent = PendingIntent.getService(this, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);

            alarmMgr.cancel(alarmIntent);

        } else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_DWELL) {
            Log.v(TAG, "GEOFENCE_TRANSITION_DWELL ");
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter != null) {
                mBluetoothAdapter.enable();
            }
            if (!mBluetoothAdapter.isEnabled())
                mBluetoothAdapter.enable();
            List triggeringGeofences = geofencingEvent.getTriggeringGeofences();
            // Get the transition details as a String.
            String geofenceTransitionDetails = getGeofenceTransitionDetails(
                    this,
                    geofenceTransition,
                    triggeringGeofences
            );

//            sendNotification(geofenceTransitionDetails);
          /*  AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent intent1 = new Intent(this, Bluetooth_BG.class);
            PendingIntent alarmIntent = PendingIntent.getService(this, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);

            alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                    1000 * 60 * 5, alarmIntent);*/

//        return alarmIntent;
//
        } else {
//            sendNotification("Error");
        }
    }

    private String getGeofenceTransitionDetails(
            Context context,
            int geofenceTransition,
            List<Geofence> triggeringGeofences) {

        String geofenceTransitionString = getTransitionString(geofenceTransition);

        // Get the Ids of each geofence that was triggered.
        ArrayList triggeringGeofencesIdsList = new ArrayList();
        for (Geofence geofence : triggeringGeofences) {
            triggeringGeofencesIdsList.add(geofence.getRequestId());
        }
        String triggeringGeofencesIdsString = TextUtils.join(", ",  triggeringGeofencesIdsList);

        return geofenceTransitionString + ": " + triggeringGeofencesIdsString;
    }


    private String getTransitionString(int transitionType) {
        switch (transitionType) {

            case Geofence.GEOFENCE_TRANSITION_ENTER:

                mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mBluetoothAdapter != null) {
                    mBluetoothAdapter.enable();
//                    Toast.makeText(getApplicationContext(), "Device support Bluetooth", Toast.LENGTH_LONG).show();
                } else {
//                    Toast.makeText(getApplicationContext(), "Device does not support Bluetooth", Toast.LENGTH_LONG).show();
                }
                if (mBluetoothAdapter.isEnabled()) {
//                    Toast.makeText(getApplicationContext(), "Bluetooth enabled", Toast.LENGTH_LONG).show();
                } else {
                    mBluetoothAdapter.enable();
//                    Toast.makeText(getApplicationContext(), "Bluetooth disabled", Toast.LENGTH_LONG).show();
                }
                return getString(R.string.geofence_transition_entered);

            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return getString(R.string.geofence_transition_exited);

            case Geofence.GEOFENCE_TRANSITION_DWELL:
                mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mBluetoothAdapter != null) {
                    mBluetoothAdapter.enable();
//                    Toast.makeText(getApplicationContext(), "Device support Bluetooth", Toast.LENGTH_LONG).show();
                } else {
//                    Toast.makeText(getApplicationContext(), "Device does not support Bluetooth", Toast.LENGTH_LONG).show();
                }
                if (mBluetoothAdapter.isEnabled()) {
//                    Toast.makeText(getApplicationContext(), "Bluetooth enabled", Toast.LENGTH_LONG).show();
                } else {
                    mBluetoothAdapter.enable();
//                    Toast.makeText(getApplicationContext(), "Bluetooth disabled", Toast.LENGTH_LONG).show();
                }
                return getString(R.string.geofence_transition_dwell);
            default:
                return getString(R.string.geofence_transition_unknown);
        }
    }


//    public class SendDistance extends AsyncTask<String,String,String>{
//
//      String url = "https://lotusbeacon.herokuapp.com/updateDevice";
//      //"http://lotusbeacon.herokuapp.com/updateDevice";
//      @Override
//      protected String doInBackground(String... params) {
//          List<NameValuePair> param = new ArrayList<NameValuePair>();
//          param.add(new BasicNameValuePair("DeviceID", regid));
//          param.add(new BasicNameValuePair("Distance", distance));
//          param.add(new BasicNameValuePair("stayTime", "1"));
//          ServiceHandler service = new ServiceHandler();
//          String response = service.makeServiceCall(url, ServiceHandler.POST, param);
//          Log.e("response", "ReceiveTransitionsIntentService SendDistance----->"+ response + "");
//          return response;
//      }
//  }


    private void sendNotification(String notificationDetails) {
        // Create an explicit content Intent that starts the main Activity.
        Intent notificationIntent = new Intent(getApplicationContext(), SplashActivity.class);

        // Construct a task stack.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        // Add the main Activity to the task stack as the parent.
        stackBuilder.addParentStack(SplashActivity.class);

        // Push the content Intent onto the stack.
        stackBuilder.addNextIntent(notificationIntent);

        // Get a PendingIntent containing the entire back stack.
        PendingIntent notificationPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // Get a notification builder that's compatible with platform versions >= 4
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        // Define the notification settings.
        builder.setSmallIcon(R.mipmap.ic_launcher)
                // In a real app, you may want to use a library like Volley
                // to decode the Bitmap.
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                        R.mipmap.ic_launcher))
                .setColor(Color.RED)
                .setContentTitle(notificationDetails)
                .setContentText(getString(R.string.geofence_transition_notification_text))
                .setContentIntent(notificationPendingIntent);

        // Dismiss notification once the user touches it.
        builder.setAutoCancel(true);

        // Get an instance of the Notification manager
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Issue the notification
        mNotificationManager.notify(0, builder.build());
    }

}


