package com.lotusempapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lotusempapp.R;
import com.lotusempapp.models.TodayFollowUP;

import java.util.List;

/**
 * Created by Nivesh on 16-Mar-17.
 */

public class TodayFollowAdapter  extends RecyclerView.Adapter<TodayFollowAdapter.RecyclerViewViewHolder> {

    private TodayFollowAdapter.RecyclerViewViewHolder viewHolder;
    private View view;
    Context ctx;
    List<TodayFollowUP> usersList;

    public TodayFollowAdapter(Context ctx, List<TodayFollowUP> usersList) {
        this.ctx = ctx;
        this.usersList = usersList;
    }

    @Override
    public TodayFollowAdapter.RecyclerViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_today_follow_up, parent, false);
        viewHolder = new TodayFollowAdapter.RecyclerViewViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TodayFollowAdapter.RecyclerViewViewHolder holder, int position) {

        holder.custName.setText(usersList.get(position).getName());
        holder.custNo.setText(usersList.get(position).getMobile_no());
        if(usersList.get(position).product_name()!=null && !usersList.get(position).product_name().equals(""))
          holder.productName.setText(usersList.get(position).product_name());
        else
          holder.productName.setText("Not available");
        if(usersList.get(position).getEmp_comments()!=null && !usersList.get(position).getEmp_comments().equals(""))
           holder.custComment.setText(usersList.get(position).getEmp_comments());
        else
           holder.custComment.setText("No comments");
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public class RecyclerViewViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView custName,custNo,custComment,productName;

        public RecyclerViewViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            productName = (TextView) itemView.findViewById(R.id.tvProductName);
            custName = (TextView) itemView.findViewById(R.id.tvCustName);
            custNo = (TextView) itemView.findViewById(R.id.tvCustMobile);
            custComment = (TextView) itemView.findViewById(R.id.tvCustComment);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
         /*//   v.getContext().startActivity(new Intent(v.getContext(),FollowupActivity.class));
            Intent _intent = new Intent(v.getContext(), IDA.class);
            _intent.putExtra("position", position);
            v.getContext().startActivity(_intent);*/
        }
    }


}
