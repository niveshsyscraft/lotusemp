package com.lotusempapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lotusempapp.R;
import com.lotusempapp.apputils.PreferenceConnector;
import com.lotusempapp.models.UserPurchasedHistory;

import java.util.List;


public class UsersInfoAdapter extends RecyclerView.Adapter<UsersInfoAdapter.RecyclerViewViewHolder> {

    private RecyclerViewViewHolder viewHolder;
    private View view;
    Context ctx;
    List<UserPurchasedHistory> usersList;
    boolean b = true;
    public UsersInfoAdapter(Context ctx, List<UserPurchasedHistory> usersList) {
        this.ctx = ctx;
        this.usersList = usersList;
    }

    @Override
    public RecyclerViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_usersinfo, parent, false);
        viewHolder = new RecyclerViewViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewViewHolder holder, int position) {
        viewHolder.productName.setText(usersList.get(position).getItemname());
        PreferenceConnector.writeString(ctx,"custname",usersList.get(position).getCustname());
        String m1 =usersList.get(position).getCustmbl1();
        String m2 = usersList.get(position).getCustmbl2();
        if(!m1.equals("")) {
            if(b) {
                PreferenceConnector.writeString(ctx, "custno", m1);
                b = false;
            }
        }
        if(!m2.equals("")) {
            if(b) {
                PreferenceConnector.writeString(ctx, "custno", m2);
                b = false;
            }
        }
        viewHolder.purcharsedDate.setText(usersList.get(position).getInvdt());
        viewHolder.productPrice.setText("");
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public class RecyclerViewViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        private final TextView productName,purcharsedDate,productPrice;

        public RecyclerViewViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            productName = (TextView) itemView.findViewById(R.id.tvProductName);
            purcharsedDate = (TextView) itemView.findViewById(R.id.tvPurcharsedDate);
            productPrice = (TextView) itemView.findViewById(R.id.tvProductPrice);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
         /*//   v.getContext().startActivity(new Intent(v.getContext(),FollowupActivity.class));
            Intent _intent = new Intent(v.getContext(), IDA.class);
            _intent.putExtra("position", position);
            v.getContext().startActivity(_intent);*/
        }
    }
}