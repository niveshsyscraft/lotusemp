package com.lotusempapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lotusempapp.R;
import com.lotusempapp.activity.UserListActivity;
import com.lotusempapp.models.Beacons;

import java.util.List;

/**
 * Created by Nivesh on 09-Mar-17.
 */

public class BeaconListAdapter extends RecyclerView.Adapter<BeaconListAdapter.RecyclerViewViewHolder>  {

    private BeaconListAdapter.RecyclerViewViewHolder viewHolder;
    private View view;
    Context ctx;
    List<Beacons> usersList;

    public BeaconListAdapter(Context ctx, List<Beacons> usersList) {
        this.ctx = ctx;
        this.usersList = usersList;
    }

    @Override
    public BeaconListAdapter.RecyclerViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_beaconslist, parent, false);
        viewHolder = new RecyclerViewViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewViewHolder holder, int position) {
        viewHolder._beaconName.setText(usersList.get(position).getBeaconKey());
        viewHolder._beaconId.setText(usersList.get(position).getBeaconID());
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public class RecyclerViewViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CardView cv;
        TextView _beaconName;
        TextView _beaconId;

        public RecyclerViewViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            cv = (CardView) itemView.findViewById(R.id.cv);
            _beaconName = (TextView) itemView.findViewById(R.id.beaconName);
            _beaconId = (TextView) itemView.findViewById(R.id.beaconId);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            Beacons beacons = usersList.get(position);
            Intent in = new Intent(ctx, UserListActivity.class);
            in.putExtra("beaconId",beacons.getBeaconID());
            in.putExtra("beaconKey",beacons.getBeaconKey());
            in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ctx.startActivity(in);
        }
    }
}
