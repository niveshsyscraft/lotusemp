package com.lotusempapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lotusempapp.R;
import com.lotusempapp.activity.UserDetailActivity;
import com.lotusempapp.models.Customer;

import java.util.List;


public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.RecyclerViewViewHolder> {

    private RecyclerViewViewHolder viewHolder;
    private View view;
    Context ctx;
    List<Customer> usersList;

    public UsersAdapter(Context ctx, List<Customer> usersList) {
        this.ctx = ctx;
        this.usersList = usersList;
    }

    @Override
    public RecyclerViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_userdetails, parent, false);
        viewHolder = new RecyclerViewViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewViewHolder holder, int position) {
        viewHolder._name.setText(usersList.get(position).getDeviceName());
        viewHolder._description.setText(usersList.get(position).getDevicePhone());
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public class RecyclerViewViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CardView cv;
        TextView _name;
        TextView _description;
        ImageView _icon;

        public RecyclerViewViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            cv = (CardView) itemView.findViewById(R.id.cv);
            _name = (TextView) itemView.findViewById(R.id.tvName);
            _description = (TextView) itemView.findViewById(R.id.tvDescription);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            Intent _intent = new Intent(v.getContext(), UserDetailActivity.class);
            _intent.putExtra("MobileNo",usersList.get(position).getMobileNo());
            _intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            v.getContext().startActivity(_intent);
        }
    }
}