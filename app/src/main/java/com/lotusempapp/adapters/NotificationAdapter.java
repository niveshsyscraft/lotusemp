package com.lotusempapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lotusempapp.R;
import com.lotusempapp.models.Notification;
import com.lotusempapp.models.TodayFollowUP;

import java.util.List;

/**
 * Created by Nivesh on 16-Mar-17.
 */

public class NotificationAdapter  extends RecyclerView.Adapter<NotificationAdapter.RecyclerViewViewHolder> {

    private NotificationAdapter.RecyclerViewViewHolder viewHolder;
    private View view;
    Context ctx;
    List<Notification> notificationList;

    public NotificationAdapter(Context ctx, List<Notification> usersList) {
        this.ctx = ctx;
        this.notificationList = usersList;
    }

    @Override
    public NotificationAdapter.RecyclerViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notification_list, parent, false);
        viewHolder = new NotificationAdapter.RecyclerViewViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.RecyclerViewViewHolder holder, int position) {

        holder.notiHeading.setText(notificationList.get(position).getHeading());
        holder.notiDate.setText(notificationList.get(position).getDate());
        holder.notiMessage.setText(notificationList.get(position).getMessage());
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public class RecyclerViewViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView notiMessage,notiDate,notiHeading;

        public RecyclerViewViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            notiMessage = (TextView) itemView.findViewById(R.id.notiMessage);
            notiHeading = (TextView) itemView.findViewById(R.id.notiheading);
            notiDate = (TextView) itemView.findViewById(R.id.notidate);
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
         /*//   v.getContext().startActivity(new Intent(v.getContext(),FollowupActivity.class));
            Intent _intent = new Intent(v.getContext(), IDA.class);
            _intent.putExtra("position", position);
            v.getContext().startActivity(_intent);*/
        }
    }


}
