package com.lotusempapp;


import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

/*import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;*/


/**
 * Created by syscraft on 1/16/2017.
 */

public class Socket_Service extends IntentService {

    //    private BeaconReferenceApplication signalApplication;
//    String baseurl = "https://lotusbeacon.herokuapp.com";


//    public String baseurl = "https://lotuslive.herokuapp.com/";

    public String baseurl = "http://192.168.1.131:5000/";
    private Socket mSocket;


    public Socket_Service() {
        super(Socket_Service.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        //CONNECT SOCKET HERE

        Log.e("Socket_Service", "onHandleIntent :--->" );
        final String BeaconID = intent.getStringExtra("BeaconID");
        String UserID = intent.getStringExtra("UserID");
//        String MobileNo = intent.getStringExtra("MobileNo");
        String Distance = intent.getStringExtra("Distance");
        Log.e("Socket_Service", "BeaconID :--->" + BeaconID);
        Log.e("Socket_Service", "UserID :--->" + UserID);
//        Log.e("Socket_Service", "MobileNo :--->" + MobileNo);
        Log.e("Socket_Service", "Distance :--->" + Distance);


        try {
            mSocket = IO.socket(baseurl);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            mSocket.connect();
            JSONObject obj = new JSONObject();
            obj.put("BeaconID", BeaconID);
            obj.put("UserID", UserID);
            obj.put("Distance", Distance);
            try {
                Log.e("getBeaconSocketService", "Socket_Service mSocket.connect() :---> " + BeaconID + " distance " + Distance);
//                obj.put("stayTime", difference);
//                                        obj.put("stayTime", "1.0");
            } catch (Exception e) {
                e.printStackTrace();
            }

            obj.put("Distance", String.valueOf(Distance));

            mSocket.emit("updateUser_Active", obj);
            mSocket.on("updateUser_Active_response", onNewMessage);
//                                    Log.e("getBeaconSocketService", "mSocket :--->" + onNewMessage);


            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.d("UI thread", "I am the UI thread");
                    Toast.makeText(getApplicationContext(), BeaconID+" Employee Beacon Detected : ", Toast.LENGTH_SHORT).show();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

//        update_device_connnectivity(BeaconID,DeviceID,MobileNo,Distance);


    }


    private Emitter.Listener onNewMessage = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            String message;
            try {
                Log.e("OnNewMessage", "Emitter.Listener message :---->" + data);
                boolean success = data.getBoolean("IsSuccess");
                message = data.getString("message");
//                Log.e("OnNewMessage", "Emitter.Listener message :---->" + message);
            } catch (JSONException e) {
                return;
            }
        }
    };


    /**
     * @param beaconID
     * @param deviceID
     * @param mobileNo
     * @param distance**************************************************************/


    private void update_device_connnectivity(final String beaconID, final String deviceID, final String mobileNo, final String distance) {


        class GetStoreAsyncTask extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {

                Log.e("doInBackground", "update_device_connnectivity :---->" );
//                String paramUsername = params[0];
//                String paramPassword = params[1];
//                System.out.println("*** doInBackground ** paramUsername " + paramUsername + " paramPassword :" + paramPassword);
       /*         HttpClient httpClient = new DefaultHttpClient();
                // In a POST request, we don't pass the values in the URL.
                //Therefore we use only the web page URL as the parameter of the HttpPost argument
                HttpPost httpPost = new HttpPost(WebServiceDetails.updateDevice_url);
                // Because we are not passing values over the URL, we should have a mechanism to pass the values that can be
                //uniquely separate by the other end.

                BasicNameValuePair usernameBasicNameValuePair = new BasicNameValuePair("BeaconID", beaconID);
                BasicNameValuePair passwordBasicNameValuePAir = new BasicNameValuePair("DeviceID", deviceID);
                BasicNameValuePair mobilenoBasicNameValuePAir = new BasicNameValuePair("MobileNo", mobileNo);
                BasicNameValuePair distanceBasicNameValuePAir = new BasicNameValuePair("Distance", distance);
                // We add the content that we want to pass with the POST request to as name-value pairs
                //Now we put those sending details to an ArrayList with type safe of NameValuePair
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(usernameBasicNameValuePair);
                nameValuePairList.add(passwordBasicNameValuePAir);
                nameValuePairList.add(mobilenoBasicNameValuePAir);
                nameValuePairList.add(distanceBasicNameValuePAir);
                try {
                    // UrlEncodedFormEntity is an entity composed of a list of url-encoded pairs.
                    //This is typically useful while sending an HTTP POST request.
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);

                    // setEntity() hands the entity (here it is urlEncodedFormEntity) to the request.
                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        // HttpResponse is an interface just like HttpPost.
                        //Therefore we can't initialize them
                        HttpResponse httpResponse = httpClient.execute(httpPost);

                        // According to the JAVA API, InputStream constructor do nothing.
                        //So we can't initialize InputStream although it is not an interface
                        InputStream inputStream = httpResponse.getEntity().getContent();

                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;
                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }

                        Log.d("UI thread", "updateDevice Api Response --- > " + stringBuilder.toString());

                        return stringBuilder.toString();

                    } catch (ClientProtocolException cpe) {
                        System.out.println("First Exception caz of HttpResponese :" + cpe);
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        System.out.println("Second Exception caz of HttpResponse :" + ioe);
                        ioe.printStackTrace();
                    }
                } catch (UnsupportedEncodingException uee) {
                    System.out.println("An Exception given because of UrlEncodedFormEntity argument :" + uee);
                    uee.printStackTrace();
                }*/
                return null;
            }

            @Override
            protected void onPostExecute(final String result) {
                super.onPostExecute(result);

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("UI thread", "I am the UI thread");
//                    Toast.makeText(getApplicationContext(), " "+ result,Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
        GetStoreAsyncTask getStoreAsyncTask = new GetStoreAsyncTask();
        getStoreAsyncTask.execute();

    }
}