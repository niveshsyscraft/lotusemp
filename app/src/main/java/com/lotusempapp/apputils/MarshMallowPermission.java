package com.lotusempapp.apputils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;


/**
 * Created by Deepika on 17-12-2015.
 */
public class MarshMallowPermission {

    public static final int LOCATION_PERMISSION_REQUEST_CODE = 4;
    public static final int Bluetooth = 5;
    public static final int Bluetooth_ADMIN = 6;
    public static final int AccessWIFIState = 3;
    public static final int WAKELOCK = 5;
    Activity activity;

    public MarshMallowPermission(Activity activity) {
        this.activity = activity;
    }

    // TODO: 22-03-2016   Microphone   permission

    public boolean checkPermissionForBlutooth(){
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.BLUETOOTH);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }
    public void requestPermissionForBluetooth(){
//        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.RECORD_AUDIO)){
//            //     Toast.makeText(activity, "Microphone permission needed for recording. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
//        } else {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.BLUETOOTH},Bluetooth);
//        }
    }

    public boolean checkPermissionForBlutoothAdmin(){
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.BLUETOOTH_ADMIN);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }
    public void requestPermissionForBluetoothAdmin(){
//        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.RECORD_AUDIO)){
//            //     Toast.makeText(activity, "Microphone permission needed for recording. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
//        } else {
        ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.BLUETOOTH_ADMIN},Bluetooth_ADMIN);
//        }
    }

    // TODO: 22-03-2016   Location   permission
    public boolean checkPermissionForLocation(){
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }
    public void requestPermissionForLocation(){
//        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)){
//            //     Toast.makeText(activity, "Microphone permission needed for recording. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
//        } else {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},LOCATION_PERMISSION_REQUEST_CODE);
//        }
    }

    // TODO: 22-03-2016   contact  permission
    public boolean checkPermissionForAccessWIFIState(){
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_WIFI_STATE);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }
    public void requestPermissionForAccessWIFIState(){
//        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_CONTACTS)){
//            //     Toast.makeText(activity, "Microphone permission needed for recording. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
//        } else {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.ACCESS_WIFI_STATE},AccessWIFIState);
//        }

    }

    // TODO: 22-03-2016   Storage  permission
    public boolean checkPermissionForWAKELOCK(){
        int result = ContextCompat.checkSelfPermission(activity, Manifest.permission.WAKE_LOCK);
        if (result == PackageManager.PERMISSION_GRANTED){
            return true;
        } else {
            return false;
        }
    }

    public void requestPermissionForWAKELOCK(){
//        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
//   //         Toast.makeText(activity, "External Storage permission needed. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
//        } else {
            ActivityCompat.requestPermissions(activity,new String[]{Manifest.permission.WAKE_LOCK},WAKELOCK);
//        }
    }
}