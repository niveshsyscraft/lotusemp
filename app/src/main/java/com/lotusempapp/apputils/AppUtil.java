package com.lotusempapp.apputils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.view.WindowManager;

import com.lotusempapp.R;

/**
 * Created by Nivesh on 14-Mar-17.
 */

public class AppUtil {

    public static void showAlert(String msg, final Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Alert").
                setMessage(msg).
                setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public static ProgressDialog createProgressDialog(Context mContext) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        try {
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
        }
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.transparentprogresdialog);
        return dialog;
    }

    public static void callPermission(Activity activity) {
        MarshMallowPermission m = new MarshMallowPermission(activity);
        if (!m.checkPermissionForAccessWIFIState()) {
            m.requestPermissionForAccessWIFIState();
        }
        if (!m.checkPermissionForBlutooth()) {
            m.requestPermissionForBluetooth();
        }
        if (!m.checkPermissionForBlutoothAdmin()) {
            m.requestPermissionForBluetoothAdmin();
        }
        if (!m.checkPermissionForLocation()) {
            m.requestPermissionForLocation();
        }
        if (!m.checkPermissionForWAKELOCK()) {
            m.requestPermissionForWAKELOCK();
        }
    }

}
