package com.lotusempapp.httputils;

public interface ResponseHandler {
    void handleResponse(String response);
}
