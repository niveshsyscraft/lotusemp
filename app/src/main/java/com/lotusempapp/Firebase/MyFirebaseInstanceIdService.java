package com.lotusempapp.Firebase;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.lotusempapp.apputils.PreferenceConnector;

/**
 * Created by Nivesh on 20-Feb-17.
 */

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String id = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG,id);
        Log.d(TAG,FirebaseInstanceId.getInstance().getToken());

        PreferenceConnector.writeString(getApplicationContext(),PreferenceConnector.FIREBASE_TOKEN,id);

//        sendToServer(id);
    }

    private void sendToServer(String id) {

    }
}
