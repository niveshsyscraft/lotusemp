package com.lotusempapp.Firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.lotusempapp.R;
import com.lotusempapp.activity.UserListActivity;

import java.util.Map;

/**
 * Created by Nivesh on 20-Feb-17.
 */

public class NotificationFromFirebase extends FirebaseMessagingService {

    private static final String TAG = "NotificationFromFirebase";
    private String title;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        try {
            Map<String, String> map = remoteMessage.getData();
            title = map.get("title");
            Log.d(TAG, remoteMessage.getFrom());
            Log.e(TAG,remoteMessage.getData()+"");
            String msg = map.get("message");
            //      String msg = remoteMessage.getNotification().getBody();
            if (map != null) {
                if (msg != null && title != null) {
                    Log.d(TAG + "<<>>", msg + title);

                    showNotification(msg, title,map.get("BeaconID"));
                } else if (msg != null && title == null) {
                    Log.d(TAG + "<<>>", msg);
                    showNotification(msg, "LotusEmployee",map.get("BeaconID"));
                } else if (msg == null && title != null) {
                    Log.d(TAG + "<<>>",  title);
                    showNotification("", title,map.get("BeaconID"));
                }
            } else {
                    showNotification("Messsage from lotus","LotusEmployee","");
            }
        }
        catch(Exception e)
        {
          e.printStackTrace();
            showNotification("","LotusEmployee","");
        }
    }

    private void showNotification(String message,String title,String id) {

        Intent intent = new Intent(this, UserListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("beaconId",id);
        PendingIntent pendingIntent = PendingIntent.getActivity
                (this,0,intent,PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri
                (RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat
                .Builder(getApplicationContext())
                .setSmallIcon(R.drawable.app_logo)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0,builder.build());
    }
}
