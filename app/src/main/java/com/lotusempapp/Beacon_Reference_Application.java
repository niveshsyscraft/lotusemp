package com.lotusempapp;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.lotusempapp.apputils.PreferenceConnector;
import com.lotusempapp.httputils.CheckNetwork;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.powersave.BackgroundPowerSaver;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;
import org.altbeacon.beacon.utils.UrlBeaconUrlCompressor;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/*import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;*/

//import com.lotus.modal.Data;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class Beacon_Reference_Application extends MultiDexApplication implements Application.ActivityLifecycleCallbacks,
        BootstrapNotifier, RangeNotifier {

    //    private static final String TAG = "Beacon_Reference_Application";
    private BeaconManager mBeaconManager;
    private Region mAllBeaconsRegion;

    private BackgroundPowerSaver mBackgroundPowerSaver;
    private RegionBootstrap mRegionBootstrap;
    private BackgroundPowerSaver backgroundPowerSaver;
    private int count = 0;
    //    private HomeScreen mainActivity = null;
    String is_welcome_msg_sent;
    private final static int NORMAL = 0x00;
    private Socket mSocket;
    private static final String TAG = "Beacon_Reference_Application";
    public static final long NOTIFY_INTERVAL = 60 * 1000; // 10 seconds

    private String enterTime;
    private String difference;

    double minimum_distance = 30.0;

//    String baseurl = "https://lotusbeacon.herokuapp.com";
//
//    {
//        try {
//            mSocket = IO.socket(baseurl);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public void onCreate() {
        super.onCreate();
        is_welcome_msg_sent = "0";
        method_for_intialization();
        IntentFilter filter1 = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mBroadcastReceiver1, filter1);
    }


    private void method_for_intialization() {
        //   FirebaseApp.initializeApp(getApplicationContext());
        mAllBeaconsRegion = new Region(getPackageName(), null, null, null);
        mBeaconManager = BeaconManager.getInstanceForApplication(this);
        mBackgroundPowerSaver = new BackgroundPowerSaver(this);
        mRegionBootstrap = new RegionBootstrap(this, mAllBeaconsRegion);
//        mBeaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("s:0-1=feaa,m:2-2=10,p:3-3:-41,i:4-20v"));
        mBeaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));
        mBeaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        mBeaconManager.setBackgroundScanPeriod(15000l);
        mBeaconManager.setBackgroundBetweenScanPeriod(5000l);
        mBeaconManager.setForegroundScanPeriod(5000l);
        mBeaconManager.setForegroundBetweenScanPeriod(5000l);
        mBeaconManager.setBackgroundMode(true);


//        mBeaconManager.setDebug(true);


//        RangedBeacon.setMaxTrackingAge() only controls the period of time ranged beacons will continue to be
//        returned after the scanning service stops detecting them. It has no affect on when monitored regions trigger exits.
//        It is set to 5 seconds by default.

//        Monitored regions are exited whenever a scan period finishes and the BeaconManager.setRegionExitPeriod()
//        has passed since the last detection. By default, this is 10 seconds, but you can customize it.


//        mBeaconManager.setMaxTrackingAge(150 * 1000);
//        mBeaconManager.setRegionExitPeriod(180 * 1000);

//        try {
//            mBeaconManager.updateScanPeriods();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, System.currentTimeMillis() + milliseconds, mWakeUpOperation);

        backgroundPowerSaver = new BackgroundPowerSaver(this);

    }


    @Override
    public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region arg1) {


        Log.d(TAG, "didRangeBeaconsInRegion--->" + getCurrentTime());
        Log.e(TAG, "didRangeBeaconsInRegion--->" + beacons.size());


        List<Beacon> bList = new ArrayList<Beacon>(beacons);
        Collections.sort(bList, new CustomComparator());

        List<String> beaconlist = PreferenceConnector.readArraylist(getApplicationContext(), PreferenceConnector.ALLOTED_BEACON_MACID_LIST);
        for (int i = 0; i < beaconlist.size(); i++) {
            Log.e(TAG, beaconlist.get(i));
        }
        if (beaconlist.isEmpty()) {

            if (CheckNetwork.isNetwordAvailable(this)) {
//                sendPostRequest("","");
                get_store_list();
            }

        }

        try {
            for (int i = bList.size(); i > 0; i--) {

                getBeaconSocketService_pk(bList.get(i - 1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void didDetermineStateForRegion(int state, Region arg1) {
//       Log.e("app->", "didDetermineStateForRegion" + state);
        Log.e(TAG, "" + getCurrentTime() + " not seeing beacons: " + state);
    }


    @Override
    public void didEnterRegion(Region region) {
        Log.e("app-> didEnterRegion = ", "time didEnterRegion--->" + getTime());

        enterTime = getCurrentTime();

        try {
            mBeaconManager.setRangeNotifier(this);
            mBeaconManager.startRangingBeaconsInRegion(mAllBeaconsRegion);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void didExitRegion(Region region) {

        try {
            difference = differenceTwoTime(enterTime, getCurrentTime());
            Log.e(TAG + " didExitRegion: ", "difference didExitRegion --->" + difference);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private final BroadcastReceiver mBroadcastReceiver1 = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        String regId = PreferenceConnector.readString(getApplicationContext(), PreferenceConnector.FIREBASE_TOKEN, "");

//                        List<String> beaconlist = PreferenceConnector.ReadArraylist(getApplicationContext(), PreferenceConnector.BEACON_MACID_LIST);
//                        try {
//                            for (String data : beaconlist) {
//                                new BeaconDisconnectedAsyncTask().execute(data, regId);
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }

                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        break;
                    case BluetoothAdapter.STATE_ON:
                        // Toast.makeText(getApplicationContext(),"Blutooth connected",Toast.LENGTH_SHORT).show();
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        method_for_intialization();
                        break;


                }

            }
        }
    };


    public void getBeaconSocketService_pk(Beacon beacon) {


        String blutoothAddress = beacon.getBluetoothAddress();
        Log.e(TAG, "getBeaconSocketService :---> blutoothAddress-->" + blutoothAddress + " Distance ---> " + beacon.getDistance() + "beacon.getServiceUuid()---> " + beacon.getServiceUuid());
//        Log.e(TAG, "getBeaconSocketService beacon.getBeaconTypeCode():--->" + beacon.getBeaconTypeCode() );
        if (!blutoothAddress.equalsIgnoreCase("")) {

            List<String> beaconlist = PreferenceConnector.readArraylist(getApplicationContext(), PreferenceConnector.ALLOTED_BEACON_MACID_LIST);
            Log.e(TAG, "getBeaconSocketService :---> Utility.beaconlist.size()-->" + beaconlist.size());

            String device_token = PreferenceConnector.readString(getApplicationContext(), PreferenceConnector.FIREBASE_TOKEN, "");

            String regId = "";
            if (beaconlist.contains(blutoothAddress)) {
                if (device_token != null && !device_token.equals("")) {
                    regId = device_token;
                }
                String url = UrlBeaconUrlCompressor.uncompress(beacon.getId1().toByteArray());
                double distance = beacon.getDistance();
                int udid = beacon.getServiceUuid();

//                String user_phoneno = SharedPrefrnceLotusApp.getSharedPrefData(getApplicationContext(), "otp_phone");

//                if (!user_phoneno.isEmpty()) {

                if (distance < minimum_distance) {

                    Intent i = new Intent(this, Socket_Service.class);
                    i.putExtra("BeaconID", beacon.getBluetoothAddress());
                    i.putExtra("DeviceID", regId);
                    i.putExtra("MobileNo", "");
                    i.putExtra("Distance", String.valueOf(distance));
                    startService(i);


//                                    try {
//                                        mSocket.connect();
//                                        JSONObject obj = new JSONObject();
//                                        obj.put("BeaconID", beacon.getBluetoothAddress());
//                                        obj.put("DeviceID", regId);
//                                        obj.put("MobileNo", user_phoneno);
//                                        try {
////              Log.e("getBeaconSocketService", "getBeaconSocketService :---> "+ beacon.getBluetoothAddress()+ " "+ regId + " stayTime " + difference + " distance " + distance);
//                                            obj.put("stayTime", difference);
////                                        obj.put("stayTime", "1.0");
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//                                        obj.put("Distance", String.valueOf(distance));
//
//                                        mSocket.emit("updateDevice", obj);
//                                        mSocket.on("updateDevice_response", onNewMessage);
////                                    Log.e("getBeaconSocketService", "mSocket :--->" + onNewMessage);
//
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }

                }
//                }
            }


        }
//        }
    }


    public boolean isapprunning() {

        ActivityManager activityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        for (int i = 0; i < procInfos.size(); i++) {
            if (procInfos.get(i).processName.equals(getPackageName().toString())) {
//                Toast.makeText(getApplicationContext(), "Lotus is running", Toast.LENGTH_LONG).show();
                return true;
            }
        }
        return false;
    }


    public String getCurrentTime() {
        //date output format
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }


    public class CustomComparator implements Comparator<Beacon> {
        @Override
        public int compare(Beacon o1, Beacon o2) {
//            if( o1.getDistance()<=(o2.getDistance())){
//                return 0;
//            }else{
//                return 1;
//            }
            return Double.compare(o1.getDistance(), o2.getDistance());
        }
    }


    public String differenceTwoTime(String startTime, String endTime) {

        String diffTime = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date1 = null;
        try {
            date1 = simpleDateFormat.parse(startTime);

            Date date2 = simpleDateFormat.parse(endTime);

            long difference = date2.getTime() - date1.getTime();
            String dif = String.valueOf(difference);
            if (!dif.contains("-")) {
                int days = (int) (difference / (1000 * 60 * 60 * 24));
                int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
                int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
                int sec = (int) (difference / 1000) % 60;
                hours = (hours < 0 ? -hours : hours);
                diffTime = hours + ":" + min + ":" + sec;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diffTime;
    }

//    private void sendNotification(String str) {
//        NotificationCompat.Builder builder =
//                new NotificationCompat.Builder(this)
//                        .setContentTitle("Beacon Detected")
//                        .setContentText(str)
//                        .setSmallIcon(R.mipmap.ic_launcher)
//                        .setStyle(new NotificationCompat.BigTextStyle().bigText(str));
//
//        long[] v = {1000, 1000, 1000, 1000, 1000};
//        builder.setVibrate(v);
//
//        builder.setAutoCancel(true);
//        Intent intent = new Intent(this, HomeScreen.class);
//        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 1, intent, 0);
//
//        builder.setContentIntent(resultPendingIntent);
//        NotificationManager notificationManager =
//                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
//
//
//        //add sound
//        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        builder.setSound(uri);
//        //vibrate
//        long[] v1 = {500,1000};
//        builder.setVibrate(v1);
//
//
//        notificationManager.notify(count, builder.build());
//    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {

        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            String message;
            try {
                boolean success = data.getBoolean("IsSuccess");
                message = data.getString("message");
//                Log.e("OnNewMessage", "Emitter.Listener message :---->" + message);
            } catch (JSONException e) {
                return;
            }
        }
    };


    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {
    }

    @Override
    public void onActivityResumed(Activity activity) {
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Intent intent = new Intent(getApplicationContext(), ReceiveTransitionsIntentService.class);
        PendingIntent.getService(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        unregisterReceiver(mBroadcastReceiver1);
    }

//    class BeaconDisconnectedAsyncTask extends AsyncTask<String, String, String> {
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            ServiceHandler jsonParser = new ServiceHandler();
//            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
//            String user_phoneno=  SharedPrefrnceLotusApp.getSharedPrefData(getApplicationContext(), "otp_phone");
//            nameValuePairs.add(new BasicNameValuePair("BeaconID", params[0]));
//            nameValuePairs.add(new BasicNameValuePair("DeviceID", params[1]));
//            nameValuePairs.add(new BasicNameValuePair("Distance", "-1"));
//            nameValuePairs.add(new BasicNameValuePair("MobileNo", user_phoneno));
//            String json = jsonParser.makeServiceCall(baseurl+"/beaconDisconnected", ServiceHandler.POST,
//                    nameValuePairs);
//            Log.e("BeaconDisconnectedAsyncTask", "beaconDisconnected--->" + json);
//            return json;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            // TODO Auto-generated method stub
//            super.onPostExecute(result);
//            Log.e("BeaconDisconnectedAsyncTask", "beaconDisconnected response--> "+result );
//
////            if (result == null || result.equals(null) || result.equals("")) {
////
////            } else {
////                Log.e("my lotus money", result + "");
////            }
//        }
//    }

    public String getTime() {
        // get date time in custom format
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(new Date());
    }

    /********************************************************************************************/

    //    Todo GPS turning on dialog:
  /*  public void sendPostRequest(String givenUsername, String givenPassword) {
        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {
                String paramUsername = params[0];
                String paramPassword = params[1];
                System.out.println("*** doInBackground ** paramUsername " + paramUsername + " paramPassword :" + paramPassword);
                HttpClient httpClient = new DefaultHttpClient();
                // In a POST request, we don't pass the values in the URL.
                //Therefore we use only the web page URL as the parameter of the HttpPost argument
                HttpPost httpPost = new HttpPost(WebServiceDetails.getbeacondata_url);
                // Because we are not passing values over the URL, we should have a mechanism to pass the values that can be
                //uniquely separate by the other end.
                //To achieve that we use BasicNameValuePair
                //Things we need to pass with the POST request
                BasicNameValuePair usernameBasicNameValuePair = new BasicNameValuePair("paramUsername", paramUsername);
                BasicNameValuePair passwordBasicNameValuePAir = new BasicNameValuePair("paramPassword", paramPassword);
                // We add the content that we want to pass with the POST request to as name-value pairs
                //Now we put those sending details to an ArrayList with type safe of NameValuePair
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(usernameBasicNameValuePair);
                nameValuePairList.add(passwordBasicNameValuePAir);
                try {
                    // UrlEncodedFormEntity is an entity composed of a list of url-encoded pairs.
                    //This is typically useful while sending an HTTP POST request.
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);

                    // setEntity() hands the entity (here it is urlEncodedFormEntity) to the request.
                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        // HttpResponse is an interface just like HttpPost.
                        //Therefore we can't initialize them
                        HttpResponse httpResponse = httpClient.execute(httpPost);

                        // According to the JAVA API, InputStream constructor do nothing.
                        //So we can't initialize InputStream although it is not an interface
                        InputStream inputStream = httpResponse.getEntity().getContent();

                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                        StringBuilder stringBuilder = new StringBuilder();

                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }

                        return stringBuilder.toString();

                    } catch (ClientProtocolException cpe) {
                        System.out.println("First Exception caz of HttpResponese :" + cpe);
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        System.out.println("Second Exception caz of HttpResponse :" + ioe);
                        ioe.printStackTrace();
                    }
                } catch (UnsupportedEncodingException uee) {
                    System.out.println("An Exception given because of UrlEncodedFormEntity argument :" + uee);
                    uee.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);

                if (result.equals("")) {
                    //Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
                } else {
                    getJsonDataOfBeacon(result);
                    //Toast.makeText(HomeScreen.this, result, Toast.LENGTH_LONG).show();
                }
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(givenUsername, givenPassword);
    }
    private void getJsonDataOfBeacon(String result) {

        Log.e("getJsonDataOfBeacon","getJsonDataOfBeacon----->" + result);

        try {

            JSONObject json1 = new JSONObject(result);
            JSONArray data_array = json1.optJSONArray("data");
//            Utility.dataList = new ArrayList<String>();
//            Utility.dataList.clear();
            ArrayList<String> mac_address_List = new ArrayList<String>();
            for (int i = 0; i < data_array.length(); i++) {
                JSONObject parentcat_obj = data_array.getJSONObject(i);
                String pId = parentcat_obj.optString("BeaconID");
//                Data obj = new Data(pId);
//                Utility.dataList.add(pId);
                if(!mac_address_List.contains(pId)) {
                    mac_address_List.add(pId);
                }
            }

            PreferenceConnector
                    .writeArraylist(getApplicationContext(),
                            PreferenceConnector.ALLOTED_BEACON_MACID_LIST,
                            mac_address_List);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    /****************************************************************/


    private void get_store_list() {


//        class GetStoreAsyncTask extends AsyncTask<String, Void, String> {
//            @Override
//            protected String doInBackground(String... params) {
////                String paramUsername = params[0];
////                String paramPassword = params[1];
////                System.out.println("*** doInBackground ** paramUsername " + paramUsername + " paramPassword :" + paramPassword);
//                HttpClient httpClient = new DefaultHttpClient();
//                // In a POST request, we don't pass the values in the URL.
//                //Therefore we use only the web page URL as the parameter of the HttpPost argument
//                HttpPost httpPost = new HttpPost(WebServiceDetails.getbeacondata_url);
//                // Because we are not passing values over the URL, we should have a mechanism to pass the values that can be
//                //uniquely separate by the other end.
//
////                BasicNameValuePair usernameBasicNameValuePair = new BasicNameValuePair("paramUsername", paramUsername);
////                BasicNameValuePair passwordBasicNameValuePAir = new BasicNameValuePair("paramPassword", paramPassword);
//                // We add the content that we want to pass with the POST request to as name-value pairs
//                //Now we put those sending details to an ArrayList with type safe of NameValuePair
//                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
////                nameValuePairList.add(usernameBasicNameValuePair);
////                nameValuePairList.add(passwordBasicNameValuePAir);
//                try {
//                    // UrlEncodedFormEntity is an entity composed of a list of url-encoded pairs.
//                    //This is typically useful while sending an HTTP POST request.
//                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);
//
//                    // setEntity() hands the entity (here it is urlEncodedFormEntity) to the request.
//                    httpPost.setEntity(urlEncodedFormEntity);
//
//                    try {
//                        // HttpResponse is an interface just like HttpPost.
//                        //Therefore we can't initialize them
//                        HttpResponse httpResponse = httpClient.execute(httpPost);
//
//                        // According to the JAVA API, InputStream constructor do nothing.
//                        //So we can't initialize InputStream although it is not an interface
//                        InputStream inputStream = httpResponse.getEntity().getContent();
//
//                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                        StringBuilder stringBuilder = new StringBuilder();
//                        String bufferedStrChunk = null;
//                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
//                            stringBuilder.append(bufferedStrChunk);
//                        }
//                        return stringBuilder.toString();
//
//                    } catch (ClientProtocolException cpe) {
//                        System.out.println("First Exception caz of HttpResponese :" + cpe);
//                        cpe.printStackTrace();
//                    } catch (IOException ioe) {
//                        System.out.println("Second Exception caz of HttpResponse :" + ioe);
//                        ioe.printStackTrace();
//                    }
//                } catch (UnsupportedEncodingException uee) {
//                    System.out.println("An Exception given because of UrlEncodedFormEntity argument :" + uee);
//                    uee.printStackTrace();
//                }
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String result) {
//                super.onPostExecute(result);
//
//                if (result.equals("")) {
//                    //Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
//                } else {
//                    getJsonDataOfStores(result);
//                    //Toast.makeText(HomeScreen.this, result, Toast.LENGTH_LONG).show();
//                }
//            }
//        }
//
//        GetStoreAsyncTask getStoreAsyncTask = new GetStoreAsyncTask();
//        getStoreAsyncTask.execute();

    }

  /*  private void getJsonDataOfStores(String result) {

        Log.e("getJsonDataOfStores", "getJsonDataOfStores----->" + result);
        ArrayList<Geofence> mGeofenceList;
        Geofence mGeofence;
        int mRadius = 3000;
        try {
            JSONObject json1 = new JSONObject(result);
            JSONArray data_array = json1.optJSONArray("data");
            Utility.StoresList = new ArrayList<Stores>();
            mGeofenceList = new ArrayList<Geofence>();
            for (int i = 0; i < data_array.length(); i++) {
                JSONObject parentcat_obj = data_array.getJSONObject(i);
                String _id = parentcat_obj.optString("_id");
                String StoreName = parentcat_obj.optString("StoreName");
                String StoreDescr = parentcat_obj.optString("StoreDescr");
                String StoreLat = parentcat_obj.optString("StoreLat");
                String StoreLong = parentcat_obj.optString("StoreLong");
                Stores store = new Stores(_id,StoreName,StoreDescr,StoreLat,StoreLong);
                Utility.StoresList.add(store);


                mGeofence = new Geofence.Builder()
                        .setRequestId("Geofence")
                        // There are three types of Transitions.
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_DWELL | Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                        // Set the geofence location and radius.
                        .setCircularRegion(Double.parseDouble(StoreLat), Double.parseDouble(StoreLong), mRadius)
                        // How long the geofence will remain in place.
//                        .setExpirationDuration((1000 * 60) * 60 * 12)
                        .setExpirationDuration(Geofence.NEVER_EXPIRE)
                        // This is required if you specify GEOFENCE_TRANSITION_DWELL when setting the transition types.
                        .setLoiteringDelay(1000)
                        .build();
                mGeofenceList.add(mGeofence);



            }

//            startGeoFencing();


        } catch (Exception e) {
            e.printStackTrace();
        }




    }*/


}


